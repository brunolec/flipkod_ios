//
//  ViewController.swift
//  flipkod
//
//  Created by Bruno Lecek on 24/10/2020.
//  Copyright © 2020 Bruno Lecek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        valueA.keyboardType = .numberPad
        valueB.keyboardType = .numberPad
        valueC.keyboardType = .numberPad
        
//        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "ResultCellTableViewCell", bundle: nil), forCellReuseIdentifier: "ReuseableCell")
    }


    @IBOutlet weak var valueA: UITextField!
    @IBOutlet weak var valueB: UITextField!
    @IBOutlet weak var valueC: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var triangles: [Triangle] = []
    
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        do{
            let triangle = try Triangle(a: valueA.text!.getDouble, b: valueB.text!.getDouble, c: valueC.text!.getDouble)
            triangles.append(triangle)
            tableView.reloadData()
        }catch{
            self.showAlert(message: "\(error)")
        }
        
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler:{ action in print(message)
        }))
        present(alert, animated: true)
    }
    
}



extension String {
    var getDouble: Double {
        return Double(self) ?? 0
    }
}


extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return triangles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReuseableCell", for: indexPath)
                        as? ResultCellTableViewCell
        cell?.labelA.text = "\(triangles[indexPath.row].a)"
        cell?.labelB.text = "\(triangles[indexPath.row].b)"
        cell?.labelC.text = "\(triangles[indexPath.row].c)"
        cell?.labelSurface.text = String(format: "%.2f", triangles[indexPath.row].calculateSurface())
        
        return cell!
    }
    
}

extension ViewController: UITableViewDelegate {
    
}

