//
//  Trianle.swift
//  flipkod
//
//  Created by Bruno Lecek on 24/10/2020.
//  Copyright © 2020 Bruno Lecek. All rights reserved.
//

import Foundation


struct Triangle{
    
    var a: Double
    var b: Double
    var c: Double
    
    
    init(a: Double, b: Double, c: Double) throws {
        if a + b > c && b + c > a && a + c > b{
            self.a = a
            self.b = b
            self.c = c
        }else{
            throw NameError.NotTriangle
        }
    }
    
    func calculateSurface() -> Double{
        let s = self.a + self.b + self.c;
        
        return sqrt(s * (s - self.a) * (s - self.b) * (s - self.c))
    }
}


enum NameError: Error{
    case NotTriangle
}

