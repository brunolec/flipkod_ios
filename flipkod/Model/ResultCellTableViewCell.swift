//
//  ResultCellTableViewCell.swift
//  flipkod
//
//  Created by Bruno Lecek on 24/10/2020.
//  Copyright © 2020 Bruno Lecek. All rights reserved.
//

import UIKit

class ResultCellTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var resultView: UIView!
    
    @IBOutlet weak var labelA: UILabel!

    @IBOutlet weak var labelB: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelSurface: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
